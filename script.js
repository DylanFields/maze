const map = [
    ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
    ['W', ' ', ' ', ' ', 'W', ' ', ' ', ' ', ' ', ' ', 'W', ' ', ' ', ' ', ' ', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', 'W', ' ', 'W', ' ', 'W', 'W', 'W', ' ', 'W', 'W', 'W', 'W', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', 'W', ' ', 'W', ' ', ' ', ' ', 'W', ' ', ' ', ' ', ' ', ' ', 'W', ' ', 'W', ' ', ' ', ' ', 'W'],
    ['W', ' ', 'W', 'W', 'W', 'W', 'W', 'W', 'W', ' ', 'W', ' ', 'W', 'W', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'W', ' ', ' ', ' ', ' ', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', 'W', 'W', 'W', ' ', 'W', 'W', 'W', 'W', 'W', ' ', 'W', 'W', 'W', 'W', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', 'W', ' ', ' ', ' ', 'W', ' ', ' ', ' ', 'W', ' ', 'W', ' ', ' ', ' ', ' ', ' ', 'W', ' ', 'W'],
    ['W', ' ', 'W', 'W', 'W', 'W', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', 'W', 'W', ' ', 'W', ' ', 'F'],
    ['S', ' ', ' ', ' ', ' ', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', 'W', 'W'],
    ['W', 'W', 'W', 'W', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', ' ', ' ', ' ', ' ', 'W', ' ', 'W', ' ', 'W', ' ', ' ', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', 'W', 'W', 'W', 'W', 'W', 'W', 'W', ' ', 'W', 'W', 'W', 'W', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
    ['W', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'W', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'W', ' ', ' ', ' ', 'W'],
    ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']
];

let pathArr = [];

let destination=document.getElementById('maze')
let player=document.getElementById('player')
let path=document.getElementById('path')

function createMap() {
    for (let row = 0; row < map.length; row++) {
        for (let column = 0; column < map[row].length; column++){
            if (map[row][column] === 'W') {
                let wall = document.createElement('div')
                wall.classList.add('wall', 'map')
                destination.appendChild(wall)
            }

            else if (map[row][column] === ' ') {
                let path = document.createElement('div')
                path.classList.add('path', 'map')
                destination.appendChild(path)
                pathArr.push(path)
            }
            else if (map[row][column] === 'S') {
                let start = document.createElement('div')
                start.classList.add('start', 'map')
                start.appendChild(player)
                destination.appendChild(start)
                pathArr.push(start)
            }
            else if (map[row][column] === 'F') {
                let finish = document.createElement('div')
                finish.classList.add('finish','map')
                destination.appendChild(finish)
                pathArr.push(finish)
            }
        }
    }

}
createMap()

function pathLoop() {
    for (let i = 0; i < pathArr.length; i++){
        pathArr[i].classList.add(i)
    }
}
pathLoop()

function createPath() { 
    let count = 0
    for (let row = 0; row < map.length; row++) {
        for (let column = 0; column < map[row].length; column++){
            if(map[row][column] !== "W"){
                map[row][column] = pathArr[count]
                count ++
            }
        }
    }
}

createPath()
console.log(map)

addEventListener('keydown', movePlayer)

function movePlayer(event) {
    Win()
    for (let row = 0; row < map.length; row++) {
        for (let column = 0; column < map[row].length; column++) {
            if (event.code === 'ArrowRight') {
                if (map[row][column].childElementCount === 1) {
                    map[row][column + 1].appendChild(player)
                    return
                }

            }
            else if (event.code === 'ArrowUp') {
                if (map[row][column].childElementCount === 1){
                    map[row - 1][column].appendChild(player)
                    return
                }
            }
            else if (event.code === 'ArrowDown'){
                if (map[row][column].childElementCount === 1){
                    map[row + 1][column].appendChild(player)
                    return
                }
            }
            else if (event.code === 'ArrowLeft') {
                if (map[row][column].childElementCount === 1){
                    map[row][column - 1].appendChild(player)
                    return
                }
            }
        }
    }
}

function Win() {
    let winner = document.getElementById('win')
    if(pathArr[84].childElementCount === 1){
        winner.innerHTML = 'You Win!'
    }
}